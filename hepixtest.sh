#!/bin/bash
## test script for the hepix RPM
## assumes 'root'

set -e

echo "Syntax checking"
for sh_file in /etc/hepix/sh/GROUP/*/group_rc.sh; do
  bash -n "$sh_file"
done
for csh_file in /etc/hepix/csh/GROUP/*/group_rc.csh; do
  tcsh -n "$csh_file"
done
if ! su --help | grep -q -- --group; then
  # SLC6 did not have this option
  echo 'Skipping per-group checks due to old "su" command'
  exit 0
fi

echo "Login checking"
useradd hepixtest ||:
mkdir -p ~hepixtest/scripts

for g in $( find /etc/hepix/sh/GROUP/ /etc/hepix/csh/GROUP/ -mindepth 1 -maxdepth 1 -printf '%f\n'| sort -u ); do
  echo "Tests for group=${g}"	
  groupadd -f "$g" ||:
  for s in /bin/bash /bin/tcsh /bin/ksh /bin/zsh; do
    hepix_out=$(su --shell="$s" --group="$g" --login --command='echo "group=$GROUP shell=$SHELL path=$PATH"' - hepixtest )
    hepix_rc=$?
    if [[ $hepix_rc -ne 0 ]]; then
      echo "ERROR: group=${g} shell=${s} returned an error ($hepix_rc)" >&2
      exit 2
    elif ! echo "$hepix_out" | grep -q 'path=.*\(/home/hepixtest/scripts\|delphi\)'; then
      # DELPHI CVMFS setup apparently strips the PATH?
      echo "ERROR: group=${g} shell=${s} does not have '/home/hepixtest/scripts' (or 'delphi') in PATH: ${hepix_out}" >&2
      exit 3
    else
      echo "   OK for shell=${s}"
    fi
  done
  # will only work if the group was added above, i.e not from LDAP
  groupdel "$g" >& /dev/null ||:
done

# cleanup:
userdel --remove hepixtest ||:
