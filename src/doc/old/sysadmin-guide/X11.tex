\section{Graphics environment}
\label{Graphics-environment}

\subsection{Introduction}
The X Display Manager, xdm, provides a way for users to log on and start
initial clients. For a user who logs in with an xdm login box the
name and the password are authenticated using the same mechanism 
as the login program. Instead of running an interactive shell, xdm
runs a series of shell scripts.  When users finish their X sessions,
xdm resets each display for the next user.  \\
Xdm also provides a way for administrators to configure environments
side-wide. It can be configured to control logins on multiple 
X Servers connected to the same machine, creating customized sessions,
and offering some basic network security features. \\ 

Xdm was introduced with X11R3 in 198?. The XDMCP, introduced in X11R4, is a
protocol shared by the xdm client and X servers throughout the network.
Using XDMCP, the X server has the responsibility of actively
requesting an xdm connection from the host. \\ 

Xdm can be configured through a set of ASCII files 
\begin{center}
\begin{tabular}{ll}
{\bf xdm-config}        & Resources specified for xdm.\\
{\bf Xservers}          & A list of servers to be explicitly managed by xdm.\\
{\bf Xsession}          & The initial startup script used by each individua X session.\\
{\bf Xresources}        & Resources to be loaded via xrdb by servers managed by xdm.\\
{\bf Xaccess}           & A file for configuring access control for XDMCP (new in X11R5).\\
{\bf GiveConsole}       & A shell script that changes the ownership of the console to the user (new in X11R5).\\
{\bf TakeConsole}       & A shell script that changes the ownership of the console back to root (new in X11R5).\\
{\bf Xsetup\_0}         & A shell script used for display setup specific to the local console server (new in X11R5).\\
\end{tabular}
\end{center}


In addition to these standard configuration files there were created
some files to customize the X environment in a heterogenous 
environment to support different keyboards and to provide X
connectivity.

\subsection{System administration files}
In addition to the standard configuration files as mentioned above there 
will be provided some special files: \\
\begin{itemize}
\item  xsession.env \\
       This file is needed for setting environment variables used by
       the Xsession script. \\
 \begin{center}
 \begin{tabular}{lp{10cm}}
 {\bf XDMPATH}  & Variable to find xdm configuration files (standard is /usr/lib/X11) \\
 {\bf BINTYPE\_SETUP}           & Variable to set the script to run architecture-specific programs \\
 {\bf HOST\_SETUP}              & Variable to set the script to run host-specific programs \\
 {\bf RESOURCES}                & Variable to set the X resource file for heavily-used clients (e.g. xterm). Resources will be 
                                  loaded to the X server resource database with xrdb. \\
 {\bf SESSIONLOG}               & Variable to switch on/off the logging of the session (date, user and display) in a file \\
 {\bf SESSIONLOGFILE}           & Variable to set the session logfile. \\
 {\bf FAILSAFE\_XTERM}          & Xterm which runs in failsafe session (e.g. xterm, hpterm, aixterm)\\
 {\bf LOGINWINDOW}              & Xterm which runs in normal session with a login shell and creates a utmp entry \\
 {\bf USER\_SESSION}            & Variable to set the script for the user Xsession (standard: \$HOME/.Xsession)\\
 {\bf USER\_RESOURCES}          & Variable to set the user X resource file which will be merged into the X server resource database \\
 {\bf WM}                       & Variable to set the window manager \\
 {\bf XTRAS}                    & Variable to set the user specification file for the additional X clients to load (standard:\$HOME/.Xtras)                   \\
 \end{tabular}
 \end{center}
\item Xsession.\$BINTYPE  Script to run architecture-specific programs specified by variable \$BINTYPE\_SETUP 
\item Xsession.\$HOST Script to run host-specific programs specified by the variable \$HOST\_SETUP 
\item sys.Xresources X resource file to set resources for heavily-used clients 
\item xsession.log Logfile if the variable \$SESSIONLOG is switched on 
\end{itemize}


The Xsession will be splitted according to the login shell flavour so that the global configuration
files : \\
\begin{tabular}{ll}
C-shell flavour         & /etc/csh.login \\
Bourne shell flavour    & /etc/zprofile or /etc/profile \\
\end{tabular}

and individual files : \\
\begin{tabular}{ll} 
[t]csh          & \$HOME/.login \\
zsh             & \$HOME/.zprofile \\
ksh,bash        & \$HOME/.profile\\
\end{tabular}
are executed.\\

The last client in the Xsession is the window manager specified with the
WM variable. If the variable is not set or the specified window
manager is not found, the Motif Window Manager as standard window
manager will be executed. You can also specify a file to load the local
window manager, for instance for X-Terminals. \\
The session ends if the user selects the logout button in the root window 
manager menu. \\

\subsection{User-specific files}
The user can specify the following  Xsession variables  
with a file called \$HOME/.xsession.env. A short example will follow
after the explanation of the variables. \\

\begin{tabular}{lp{8cm}}
{\bf USER\_RESOURCES}  & Variable to set the user X resource file which will be merged into the X server resource database (standard: \$HOME/.Xresources) \\
{\bf LOGINWINDOW}      & Xterm which runs in the normal session with a login shell and creates an utmp entry\\
{\bf WM}               & Variable to set the window manager \\
{\bf XTRAS}            & Variable to set the user specification file for the additional X clients to load (standard:\$HOME/.Xtras) Here is a short example file.
\begin{verbatim}
                         if [ $BINTYPE = "SunOS" ]; then   
            /usr/local/bin/X11/xclock &
            /usr/local/bin/X11/xsetroot -solid green &
            /usr/local/bin/X11/xterm &
         else
            /usr/bin/X11/xclock &
            /usr/bin/X11/xsetroot -solid blue &
            /usr/bin/X11/xterm &
         fi
\end{verbatim} \\
{\bf USER\_SESSION}    & Variable to set the script for the user Xsession (standard: \$HOME/.Xsession) This user-specific startup script will be executed by the system-wide Xsession script after the loading 
                         of the Xresources, the keyboard handling and the login procedure which set the login environment. The user has to ensure that the last X client he starts runs in the foreground. \\
\end{tabular}
\\
\\

\underline{Short example for \$HOME/.xsession.env file: \\ } 
\\
\begin{verbatim}
XTRAS=$HOME/.Xclients
if [ $BINTYPE = "SunOS" ]; then
    WM=/usr/openwin/bin/olwm
else
    WM=/usr/bin/X11/mwm
fi
\end{verbatim}


A summary of the dynamic behaviour of how the system sets the X
environment through the control files is given in figure
\ref{X-dynam-summary}.

\begin{center}
\begin{figure}[H]
  \mbox{
\epsfig{figure=xdynamic.eps,height=0.7\textheight}}
 \caption{Dynamic behaviour of the X environment}
 \label{X-dynam-summary}
\end{figure} 
\end{center}

\subsection{Keyboard support}
Currently there is no way to detect the keyboard type automatically.
In order to provide a keyboard support we will provide a tool called
{\bf special.keyboard} which is called from the global {\bf Xsession} file.
The file looks for a map file {\bf .keyboard} in the user home directory or
a global file {\bf special.keyboard\_list} in {bf \$XDMPATH/options}.
If these files do not exist or there is no entry for the specific displays
then this tool queries the vendor of the X server with an xrdb -symbols
and will assume you have an standard keyboard from this vendor. \\
An additional tool xguesskbd which uses the public domain program
xkeycaps can create an entry in the map files. The script starts the
xkeycaps program with the guessed keyboard. If this is not the
correct keyboard layout the user can select another keyboard. After 
finishing the xkeycaps program he will get a list of supported keyboard
types and can set this entry by hand otherwise the guessed keyboard type will
be set automatically in the map files. \\ 

The keyboard names in Appendix A should be used. \\ 

\underline{The map file should have the following syntax:}\\

Xterminalname:keyboardname \\
eg.:\\
xtcnsw07.cern.ch:NCD101 \\


For each supported keyboard the system administrator can create a \\
file \$XDMPATH/options/special.\$KEYBOARD where 
he can run commands like xmodmap or xrdb to customize the 
specific keyboard. If the user wants to customize his terminal he 
has to create a file \$HOME/.keyboard.\$KEYBOARD. \\
An example for such a file looks as follows: \\
\begin{verbatim}
#!/bin/ksh
# Define 'Compose' key as Alt key
/usr/bin/X11/xmodmap - << !
clear   Mod1
keycode 33       =      Meta_L
add     Mod1     =      Meta_L
keycode 110      = BackSpace Delete
!
/usr/bin/X11/xrdb -merge <<!
vaxterm.vt100.Translations: #override \
        <Key>BackSpace:         string(0x7f) \n\
!
\end{verbatim} 

\subsection{Tools for X connectivity}
In order to start X clients on remote hosts it is necessary to
transfer the DISPLAY environment variable. There are two recommended tools 
to do this. \\
\begin{itemize}
\item {\bf xrsh} starts an X11 client on another host \\ 
Usage: xrsh [-l username] [-auth authtype] [-screen \#] [-pass envlist] 
       [-debug] [-debug2] hostname [X-command [arguments...]] \\ 
This tool runs the given command on another host with \$DISPLAY set in the 
environment and handles authentication based on the value of \$XRSH\_AUTH\_TYPE 
or the value of the -auth command line switch.\footnote{See the X-Security section}
\item {\bf xrlogin} executes a rlogin call with the transfer of the DISPLAY variable.
This variable will be packed together with the TERM variable and has to be
unpacked on the remote site. The HEP startup login scripts support this 
unpacking. \\
\end{itemize}
There are also tools with a special key mapping. \\
\begin{itemize}
\item {\bf vaxterm} start an xterm with a VT mapping. \\
                   For some special vms editors the german language is
                   supported in the following way: \\
           \begin{verbatim}
              <Alt>a     maps to a "dotted" a
         Shift<Alt>a     maps to a "dotted" A
              <Alt>o     maps to a "dotted" o
         Shift<Alt>o     maps to a "dotted" O
              <Alt>u     maps to a "dotted" u
         Shift<Alt>u     maps to a "dotted" U
             <Alt> s     maps to a "sharp" S
           \end{verbatim}
                   Before this mode can be used a user must type : \\
                        \verb+
                           set terminal/eight+ \\
                   on the vms system.

\item {\bf x3270} opens a telnet connection to an IBM host in an X
          window. 
\end{itemize}

 
\subsection{Window Manager Configuration}
In order to have a common behaviour the window manager should be 
configured in a standardized way. \\ 

\underline{\bf Button Bindings} \\

It is useful to customize a set of button bindings that are used to 
configure window manager behaviour.  A window manager function can be 
assigned to situations when a button press occurs  with the pointer 
positioned over a framed client window, an icon, or the root window.   \\
A Motif Window Manager configuration can be found in Appendix B. \\


\subsection{X-Security}
X runs in a networked environment and because of the X11 design, the
workstation you are working on can be accessed by any other host on
the network. So the possibilities for abuse are considerable. If your
environment is not protected in the proper way, an intruder can ``xmelt'' your
screen, send you funny (or not) pictures via ``xsetroot'' or spy what
you are doing, capture your passwords or take control of your mouse or your
keyboard.\\


We have to distinguish between host and user-based access control
mechanism. The xhost client can be used to give (or deny) remote
systems access to the X-Server interactively. But other people 
working on this remote system can also access your display. \\
A far better way is to use the user-based access control introduced
with X11 Release 4. The most common method of user-based access
control is the mechanism known as MIT-MAGIC-COOKIE. X11 Release 5
provides two new schemes: XDM-AUTHORIZATION-1 and SUN-DES-1. \\
Another way for display access control uses the public domain
tool mxconns. It creates a virtual X server running on the  specified
display and prompts the user for each new connection, asking
whether the connection should be accepted or not.
All those systems are studied in \cite{O-Reilly92} and \cite{Cons93b}.


