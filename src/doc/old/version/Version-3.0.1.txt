HEPiX Shells and Login scripts version 3.0.1

-------------------------------------------------------------------------------
This is a bug fix version of the HEPiX scripts package.

For a list of features in the 3.0.* HEPiX scripts consult:

        http://wwwcn.cern.ch/hepix/wg/scripts/www/Welcome.html

in the Versions section.

This version fixes very minor bugs/features:

   *  Shell scripts:

        o  the MANPATH variable has now the pattern:

                          HOME:SUE:SYSTEM:/afs/usr/man:ASIS:CERNLIB

          note that /afs/usr/man comes after the system MANPATH.
        o  aliases files were called twice.
        o  in tcsh the showdots parameter is now set to '-A'. This means that
          in file name generation . and .. won't appear.
        o  there was a bug on the AIX PATH which had been fixed.
        o  the variable MAILSERVER=mail.cern.ch is not introduced.
        o  some tiny bugs had been fixed in the debug mode.
        o  the zsh prompt is now compatible with the new version of zsh.

   *  X11 scripts:

        o  the X11 scripts were providing a bad configuration for the (very
          bad) PC Xvision Xserver (use NCD PCXware instead because it is much
          better).
        o  the X font path is not set for the PC Xvision Xserver because the
          latter doesn't support this operation.
        o  the central_xsession file calls an explicit ksh instead of sourcing
          a file. This allows users to use a special HEPiX .xinitrc file

                          /usr/local/lib/hepix/templates/xsession

          to get the HEPiX environment even if xdm is not running.
        o  a system or cluster system administrator can now customise some
          caracteristics of the X login panel through

                          /etc/hepix/Xresources

          and

                          /etc/hepix/cluster/Xresources

        o  adam is not the last client but for PC Xware Xsessions. The window
          manager is the last client in all ohter cases.

   *  tools:

        o  the tklife script had a minor bug (tokens was not available in the
          PATH).
        o  the uco command menus where not allowing the Cancel item number 0.

   *  templates:

        o  After a user suggestion, another way to define LPDEST had been
          added:

                      # setenv LPDEST $PRINTER         # if PRINTER is defined, use it!

          or

                      # LPDEST=$PRINTER; export LPDEST # if PRINTER is defined, use it!

          as an example in the templates.
        o  the path to the quota-warn command was wrong. quota-warn in under
          /usr/local/lib/hepix/tools of course.

Contact: Arnaud.Taddei@cern.ch
-------------------------------------------------------------------------------
Arnaud Taddei, 22-Nov-1995
