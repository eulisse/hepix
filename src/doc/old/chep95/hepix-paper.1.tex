\documentstyle[mprocl,psfig,html,longtable,epsfig,rotating]{article}

\bibliographystyle{unsrt}    % for BibTeX - sorted numerical labels by order of
                             % first citation.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                %
%    BEGINNING OF TEXT                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{Common HEP UNIX Environment}

\author{ Arnaud Taddei }

\address{CERN, CN Division, DCI Group\\
CH 1211 Gen\`{e}ve 23, SWITZERLAND}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% You may repeat \author \address as often as necessary      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\maketitle
\abstracts{After it had been decided to design a common user
environment for UNIX platforms among HEP
laboratories, a joint project between DESY and CERN
had been started. The project consists in 2 phases: \\
\\
   1. Provide a common user environment at shell level, \\
   2. Provide a common user environment at graphical
     level (X11). \\
\\
Phase 1 is in production at DESY and at CERN as
well as at PISA and RAL. It has been developed around the
scripts originally designed at DESY Zeuthen improved and extended
with a 2 months project at CERN with a contribution
from DESY Hamburg. It consists of a set of files which
are customising the environment for the 6 main shells (sh,
csh, ksh, bash, tcsh, zsh) on the main platforms (AIX,
HP-UX, IRIX, SunOS, Solaris2, OSF/1, ULTRIX, etc.)
and it is divided at several "sociological" levels: HEP,
site, machine, cluster, group of users and user with some
levels which are optional.\\ 
\\
The second phase is under design and a first proposal has
been published. A first version of the phase 2 exists already
for AIX and Solaris, and it should be available for all 
other platforms, by the time of the conference.\\
\\
This is a major collective work between several HEP
laboratories involved in the HEPiX-scripts and
HEPiX-X11 working-groups.\\
}

\section{Introduction and Background}
When a user interacts with a system, he will be in interaction with mainly 4 
kinds of interfaces: 
\begin{itemize}
\item shells ({\bf sh}, {\bf csh}, {\bf ksh}, {\bf bash}, {\bf tcsh}, 
      {\bf zsh})
\item graphical system (X11)
\item applications ({\bf pine}, {\bf emacs}, etc.)
\item connectivity tools ({\bf rsh}, {\bf xrsh}, {\bf telnet}, etc.)
\end{itemize}
In UNIX systems, many shells, a graphical
system namely X11, thousands of applications which can be customised 
in several ways and eventually connectivity issues between systems 
make the life uneasy for a end user.

Table \ref{combinatorics} tries to give readers and idea of the complexity of
the UNIX environments:

$\chi = M . G . W . S . K . Cxt . Tty . O$

Where:

{\small
\begin{longtable}{|p{15mm}|p{5cm}|p{5cm}|}
\caption{Combinatorics elements} \label{combinatorics}\\
\hline
{\bf Symbol} & {\bf Definition} & {\bf Value} \\
\hline
$M$       & Number of machines and UNIX platforms.
          & AIX, HP-UX, SunOS, Solaris 2, IRIX, ULTRIX, OSF/1, Convex, 
            (Linux ?).\\ 
\hline
$G$       & Number of graphical interfaces.
          & Plain Motif, Openwin and desktops like COSE and HP-VUE.\\
\hline
$W$       & Number of possible Window Managers.
          & {\tt mwm}, {\tt twm}, {\tt fvwm}, {\tt dtwm}, etc. \\
\hline
$S$       & Number of shells.
          & {\tt sh}, {\tt csh}, {\tt ksh}, {\tt bash}, {\tt tcsh}, 
            {\tt zsh}.\\
\hline
$K$       & Number of keyboard types.
          & PC-like, NCD-like, VT-like, HP-like, SunOS-like, etc.\\
\hline
$Cxt$     & Number of possible context of call.
          & {\tt telnet}, {\tt rlogin}, {\tt login}, {\tt su}, {\tt su -}, 
            etc.\\
\hline
$Tty$     & Number of possible terminal and pseudo-terminals.
          & {\tt VT100}, {\tt VT200}, {\tt xterm}, etc.\\
\hline
$O$       & Other factors like the impact of the network, etc.
          & \\
\hline
\end{longtable}
}
Thus, $\chi$ shows that you will deal with a big number of different 
\underline{behaviours}.
\\
In front of this "disaster", the HEPiX commitee (High Energy Physics 
UNIX commitee), acknowledged that the situation was unacceptable for users
because they were facing too many different interfaces and it costs them a lot
of time to customise the systems the way they like if it is even possible!

Thus, the HEPiX scripts project had been launched and this paper describes the
motivations, and some components 
from the work which mainly involved the DESY UNIX Commitee (DUX) and
CERN itself. This project is meant to have a more homogeneous environment
in analogy to the HEPVM efforts on the IBM mainframes. 

So, one goal is to keep differences between platforms as small as possible
and the challenge is to ensure that all combinations of platforms and software 
products represent a reliable and consistent user environment.
\\
These scripts are setting up a Common Unix Environment at shell 
and graphical level (X11) (available in beta version now).

At shell level, they provide the default environment for users to login, work 
in interactive and batch mode for the most common shells and platforms.

At X11 level, they provide the default environment for users to login from 
an Xserver (X terminal, Workstation, PC, etc.) to a UNIX system with a 
properly set up "Xsession".

Thus each UNIX user should be able to work on any platform in the HEP
community without having to reset his environment files.
\\
In order to reach this not modest goal, DESY Hamburg and DESY Zeuthen started
the initial work on the shell level in 1992. They produced the main engine
and the main background ideas. Then a joint project between DESY Zeuthen 
and CERN had been done between February and end of March 1994. Since then
\begin{itemize}
\item the HEPiX scripts at shell level had been deployed at CERN, RAL, INFN,
\item the HEPiX scripts group worked on the X11 specifications and 
      implementation and new work had been produced between DESY Hamburg, 
      DESY Zeuthen and CERN.
\end{itemize}




\section{Components}

\subsection{Overview of the components}
The HEPiX scripts are a set of files which you can install on your system in
different modes. These files provides settings for shell configurations
and in a near future for X11 environment configurations. Moreover, the "HEPiX
package" contains documentation for users, system administrators, etc.
who want to use and/or customise the environment. Besides the documentation,
it provides templates for the user dot-files and tools like the {\bf uco} 
command which can be used as a "reset button" for users to reset their 
environment to the system defaults. At least, in order to provide some 
Software Quality Assurance, the HEPiX scripts are compiled, available in debug
mode, optimised for each target, post-processed for better readibility.

\subsection{Shells}
The shells are the "oldest" interface with the system. When you want to 
interact with an operating system (not restricted to UNIX) you enter commands
in the "shell" which executes them. In UNIX it exists many shells and the
most classical ones are: {\bf sh}, {\bf csh}, {\bf ksh}, {\bf bash}, 
{\bf tcsh}, {\bf zsh}. \cite{taddei.shellchoice} gives a comparison 
between them. The problem is that they have many different behaviours, unequal
support, sometime big defficiencies and bugs ({\bf sh}, {\bf ksh}, {\bf csh}).

Any improvement to it can result in considerably more effective use of the 
system.

The HEPiX scripts support all of these shells and enable by default, 
the most advanced features of each of them.
Command line editing, shell history and completion mechanism are some examples
of such features.
Morevoer, they provide some keybindings, they define few common aliases
and environment variables are set properly.

The shell status is detected and specific environments are provided according
to it. If the shell is a login shell, some additional files may be started
and thus the login environment is calculated once. In interactive mode, 
some environment is provided as well as in any non-interactive environment
(subshells, batch, etc.). The environment is still correct after changing the
shell, after exiting from an "{\bf su}" session or for remote commands like
{\bf rsh}.

\subsection{levels}

More fundamental is the role that the HEPiX scripts are playing as a tool 
which can be customised by various kinds of system administrators. In 
big organisations such as CERN it had been necessary to distinguish among
the system administrators.

\begin{itemize}
\item A user in front of a workstation
\item Somebody responsible of a group of users
\item Somebody responsible of a group of machines
\item A manager
  \begin{itemize}
    \item who has political responsibility 
    \item who wants to implement some servives (improve the security)
    \item please some users communities
  \end{itemize}
\end{itemize}

Thus, the HEPiX scripts provide hooks or anchors for some predefined
files which can be customised by any of these system administrators. 
Figure \ref{mechanical} shows the "route" which is used by the shell to get
its configuration.

\begin{figure}
\caption{Levels - Mechanical aspects} \label{mechanical}
\makebox{\resizebox{\textwidth}{!}{\rotatebox{0}{\includegraphics{fig/mechanical-levels-light.ps}}}}
\end{figure}

A key point on figure \ref{mechanical} is to observe that there are 
two installation modes.
The "{\it weak}" mode which doesn't replace any vendor system file and which is
meant for all machines which provide "{\it old}" services. 
For these machines it
would be probably not very convenient to enforce a new environment for all
these users. The "{\it enforced}" mode is used for all new machines, 
services, users
and group of users. Thus, they get a default HEPiX environement. This
mechanism gives the flexibility to migrate slowly services to the HEPiX 
environment.
Moreover, it should be pointed out on the fact that {\tt root} and all users
with a {\tt uid} less than 100 are not getting the HEPiX environment by 
default. This is shown with the anchor to the box "{\it vendor}" on figure
\ref{mechanical}

\subsection{X11}

This paper is not the correct place to describe the HEPiX X11 feature. This
will be explained in a paper "Architectural Design Documentation of the 
HEPiX scripts". 

\section{Project status \& Future Plans}

This project started in 1992 with an initial research and development 
lead by DESY Hamburg then Zeuthen. At the beginning of 1994, Axel K\"ohler
came at CERN and with Arnaud Taddei they improved the scripts at shell
level, provided documentation, started the initial deployment. After the 
initial feedback at CERN, A. Taddei worked on new developments of the 
HEPiX scripts and started the major deployment during winter 1994-1995.
In the same time, RAL and INFN started to deploy the HEPiX scripts on their
respective sites.

Although the scripts have been really well accepted, it became clear that 
solving the shell problem was not enough and A. Taddei set up 2 working-groups
one at CERN and another in the HEPiX project in order to specify and then
propose a first implementation. 

Figure \ref{people} shows the number of people involved as figure 
\ref{history} summarise the history of the project. It has to be said here
that the cooperation between DESY and CERN and the feedback from INFN, RAL
Saclay and Slac is really a key-factor of success. Many people contributed
to provide users a decent defaults and system managers easy ways to make
local adaptations.


\begin{figure}
\caption{People involved} \label{people}
\makebox{\resizebox{\textwidth}{!}{\rotatebox{0}{\includegraphics{fig/hepix-people.ps}}}}
\end{figure}

\begin{figure}
\caption{Project history} \label{history}
\makebox{\resizebox{\textwidth}{!}{\rotatebox{0}{\includegraphics{fig/hepix-history.ps}}}}
\end{figure}

At CERN it should be noticed that the HEPiX scripts are deployed on all the
work group servers and PLUS machines. Thus, CMS, ATLAS, SHITCMS, SHIFTATLAS,
CERNSP, HPPLUS and even some "old" systems like DXCERN provide the HEPiX
scripts. The integration with the other services had been succesfull, in
particular, with SUE, ASIS and CORE.




\section{Conclusion}

The HEPiX scripts provide a very powerful paradigm. They provide decent
defaults, they allow a lot of evolutions and meet migration requirements.
They show a very good acceptance level, at CERN and in otjer HEP labs. 
Eventually, they generated the development of a HEP-wide X11-environment
and they will allow us to integrate and support a lot of applications.

The shell part is considered to be finished and we definitely are in its
maintenance phase. The X11 part of the HEPiX scripts is available in beta
version and is under restricted deployment and should be available in a near
future.





\section*{References}
\begin{thebibliography}{99}
\bibitem{www.shells}{\em HEPiX Shell scripts welcome page}, \\
             Arnaud Taddei,\\
             \htmladdnormallink{{\tt http://wwwcn.cern.ch/hepix/wg/shells/www/Welcome.html}}{http://wwwcn.cern.ch/hepix/wg/shells/www/Welcome.html}.
\bibitem{www.X11}{\em HEPiX X11 scripts welcome page}, \\
             Arnaud Taddei,\\
             \htmladdnormallink{{\tt http://wwwcn.cern.ch/hepix/wg/X11/www/Welcome.html}}{http://wwwcn.cern.ch/hepix/wg/X11/www/Welcome.html}. 
\bibitem{www.X11}{\em UMTF X11 welcome page}, \\
             Arnaud Taddei, Miguel Marquina\\
             \htmladdnormallink{{\tt http://wwwcn.cern.ch/umtf/wg/X11/www/Welcome.html}}{http://wwwcn.cern.ch/umtf/wg/X11/www/Welcome.html}. 
\bibitem{www}{\em ADAM WWW Home Page},\\
             Miguel Li\'{e}bana, Miguel Marquina,\\
             \htmladdnormallink{{\tt http://wwwcn.cern.ch/umtf/adam.html}}{http://wwwcn.cern.ch/umtf/adam.html}.
\bibitem{taddei.shellchoice}Shell-Choice - A Shell Comparison, \\
                Arnaud Taddei, \\
                CERN Writeup: CERN/CN/UCO/162 \\
                \htmladdnormallink{{tt http://consult.cern.ch/writeup/shellchoice}}{http://consult.cern.ch/writeup/shellchoice}.
\bibitem{defert}ASIS: User's and Reference Guide, \\
                Philippe Defert, A. Peyrat, I. Reguero \\
                CERN Writeup: CERN/CN/UCO/152 \\
                \htmladdnormallink{{\tt http://consult.cern.ch/writeup/asis}}{http://consult.cern.ch/writeup/asis}.
\bibitem{taddei}Proposal for a Common Script and X environment at DESY
                and at CERN,\\
                Arnaud Taddei.\\ 
                CERN Writeup: CERN/CN/UCO/168 \\
                \htmladdnormallink{{\tt http://consult.cern.ch/writeup/hepscripts}}{http://consult.cern.ch/writeup/hepscripts}.
\bibitem{austen94}The Common Desktop Environment,\\
                  Rebecca F. Austen.\\
                  AIXpert August 94.\\
                  \htmladdnormallink{{\tt http://www.austin.ibm.com/developer/aix/library/aixpert/aug94/aug94toc.html}}{http://www.austin.ibm.com/developer/aix/library/aixpert/aug94/aug94toc.html}
\end{thebibliography}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%
% End of mprocl.tex  %
%%%%%%%%%%%%%%%%%%%%%%


%%+++++++


