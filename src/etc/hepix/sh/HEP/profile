#!/bin/sh

[ -n "$_HPX_SEEN_HEP_PROFILE" ] && return
_HPX_SEEN_HEP_PROFILE=1

[ -z "$HPX_INIT" ] && . /etc/hepix/init.sh

hpx_source $HPX_HOME_HEP/env

[ -f /etc/nologin ] && [ "$USER" != "root" ] && {
	hpx_echo "N: Sorry, system is under maintenance, no logins allowed."
	/bin/cat /etc/nologin
	hpx_echo "N: Sleeping 5 seconds."
	/bin/sleep 5
	exit
}

[ -s /etc/hepix/motd ] && /bin/cat /etc/hepix/motd

# Checks AFS quota.  It has to be a function since the shell is doing
# the parameter parsing for us, because sed(1) gives us the numbers in
# one line, separated by spaces.
_hpx_afs_quota_check () {
	# volsize used %used %partition
	hpx_debug /usr/bin/expr $1 - $2
        [ -z $1 ] && return
        [ -z $2 ] && return
        [ -z $3 ] && return
	_HPX_LEFT=`/usr/bin/expr $1 - $2`
	[ $_HPX_LEFT -le 500 ] && {
		hpx_echo "W: Your home directory has only ${_HPX_LEFT}KB left."
		hpx_echo "W: This may cause problems, especially when"
		hpx_echo "W: logging in.  Please remove unwanted files"
		hpx_echo "W: or go to http://cern.ch/account"
		hpx_echo "W: to increase your quota."
		return
	}
	[ $3 -ge 95 ] && {
		hpx_echo "W: Your home directory is ${3}% full.  This may"
		hpx_echo "W: cause problems, especially when logging in."
		hpx_echo "W: Please remove unwanted files, or go to"
		hpx_echo "W: http://cern.ch/account to increase your quota."
		return
	}
	unset _HPX_LEFT
}

if [ -n "$HPX_AFS_HOME" ]; then
	[ -x /usr/bin/fs ] && HPXB_FS=/usr/bin/fs
	if [ -n "$HPXB_FS" ]; then
		# Execute fs lq ~ command and extract numbers from output.
		# The shell could do the whitespace split but fs(1) is
		# altering output on quota-near-full conditions.
		_HPX_LQ=`$HPXB_FS lq $HOME 2>/dev/null`
		
		# fs(1) would complain loudly of access violation if
		# logging in without a token.
		if [ $? -eq 0 ]; then
			hpx_debug $_HPX_LQ
			_HPX_LQ=`echo $_HPX_LQ | /usr/bin/xargs | \
				/bin/sed -n '1s/^[^.]*\.[^ ]* [^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\)%[^0-9]*\([0-9]*\)%.*$/\1 \2 \3 \4/p'`
			hpx_debug $_HPX_LQ
			_hpx_afs_quota_check $_HPX_LQ
		else
			hpx_echo "E: $HPXB_FS returned error, no tokens?"
		fi
		unset _HPX_LQ
	else
		hpx_echo "E: Unable to check AFS quota, no fs(1) binary?"
	fi
fi

# Personal temporary directory.
HPX_TMPDIR=/tmp/$USER
set -a
[ -d $HPX_TMPDIR ] && [ -O $HPX_TMPDIR ] && TMPDIR=$HPX_TMPDIR
if [ -z "$TMPDIR" ]; then
        HPX_UMASK=`umask`
	umask 0077
	/bin/mkdir $HPX_TMPDIR >/dev/null 2>&1 && TMPDIR=$HPX_TMPDIR
	umask $HPX_UMASK
        unset HPX_UMASK
	[ -n "$TMPDIR" ] && hpx_debug "Created $HPX_TMPDIR."
fi
set +a
unset HPX_TMPDIR

# End of file.
