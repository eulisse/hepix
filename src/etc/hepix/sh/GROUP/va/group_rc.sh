# https://its.cern.ch/jira/browse/NOAFS-495
## shell settings for "va" (AMS)

OS="Linux"; export OS
LD_LIBRARY_PATH="/afs/cern.ch/ams/local2/opt/xrootd-gcc64-41/lib64"; export LD_LIBRARY_PATH
if [ -d /opt/intel/Compiler/11.0/069/ ]; then
  PATH="/opt/intel/Compiler/11.0/069/bin/ia32:${PATH}"; export PATH
  LD_LIBRARY_PATH="/opt/intel/Compiler/11.0/069/lib/ia32:${LD_LIBRARY_PATH}"; export LD_LIBRARY_PATH
fi

STAGE_HOST=castorpublic; export STAGE_HOST
STAGE_SVCCLASS=amsuser; export STAGE_SVCCLASS
CASTOR_INSTANCE=castorpublic; export CASTOR_INSTANCE

ulimit -m 2000000
case "$USER" in
  ams)
    ulimit -m 4000000
    ;;
  merge)
    ulimit -S -v 6000000
    ulimit -m 2000000
    ;;
  qfeng)
    ulimit -S -v 8000000
    ulimit -S -m 4000000
    ;;
esac
