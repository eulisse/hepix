# group settings for 'ws' (OPAL)
# https://its.cern.ch/jira/browse/NOAFS-501
# based on /afs/cern.ch/group/ws/group_env.sh

export OPAL=/afs/cern.ch/opal     ##Define the top of the OPAL project tree
export OPAL_ROOT=$OPAL/offline    ##Define the top of the OPAL code tree
export OPAL_WWW=$OPAL/www         ##Define the top of the OPAL WWW tree
export OPAL_GEN=$OPAL/offline/generator ##Define the top of the 
                                            ##OPAL generator tree
export otop=$OPAL/offline ##Define the top of the OPAL code tree
export odoc=$OPAL/www/doc ##Define the top of the OPAL documentation
export opal=$OPAL     ##Define the top of the OPAL project tree
export OPAL_GR=$OPAL/offline/grope ## the grope directory

CERN_ROOT=/cern/pro; export CERN_ROOT

## Define CERN global variables
libver="pro" ;                            export libver
gksver="pro" ;                            export gksver
cern=/cern ;                              export cern
cern_root=$cern/$libver ;                 export cern_root
kernlib=$cern_root/lib/libkernlib.a ;     export kernlib
packlib=$cern_root/lib/libpacklib.a ;     export packlib
mathlib=$cern_root/lib/libmathlib.a ;     export mathlib
graflib=$cern_root/lib/libgraflib.a ;     export graflib      
graflibX11=$cern_root/lib/libgrafX11.a ;  export graflibX11

## Generate OPAL symbols <version_xx>
version_opal=pro ; export version_opal
if [ -f $otop/utl/oversion.$version_opal ]; then
        hpx_source $otop/utl/oversion.$version_opal
else
        echo "Warning: oversion.$version_opal does not exist!"
fi

OPAL_P_CAR=$otop/links/pro
OPAL_P_CRA=$otop/links/pro
OPAL_P_LIB=$otop/links/pro
OPAL_D_CAR=$otop/links/dev
OPAL_D_CRA=$otop/links/dev
OPAL_D_LIB=$otop/links/dev

OPAL_P_DB=$otop/db

export OPAL_P_CAR OPAL_P_CRA OPAL_P_LIB OPAL_D_CAR OPAL_D_CRA OPAL_D_LIB OPAL_P_DB

CERN_P_CAR=/cern/pro/src/car; export CERN_P_CAR
CERN_P_LIB=/cern/pro/lib; export CERN_P_LIB

FMOPAL=$OPAL/fatmen/fmopal ; export FMOPAL

HARDWARE=`$otop/scripts/hardware`
export HARDWARE
if [ "$HARDWARE" = "i386slc3" ]; then
  OpalBinTag='/i386slc3'
elif [ "$HARDWARE" = "i386slc5" ]; then
  OpalBinTag='/i386slc5'
elif [ "$HARDWARE" = "amd64slc5" ]; then
  OpalBinTag='/amd64slc5'
elif [ "$HARDWARE" = "i386slc6" -o "$HARDWARE" = "amd64slc6" ]; then
  OpalBinTag='/i386slc6'
  PATH=$PATH:/cern/pro/bin
elif [ "$HARDWARE" = "i386cc7" -o "$HARDWARE" = "amd64cc7" ]; then
  OpalBinTag='/i386cc7'
  PATH=$PATH:/cern/pro/bin
else
  OpalBinTag=''
fi

PATH="${otop}${OpalBinTag}/mgr:$otop/scripts:$otop/bin:$PATH"; export PATH

propath=". $HOME/car $HOME/cra $HOME$OpalBinTag/lib $HOME$OpalBinTag/src $otop/rope/pro/car $otop/rope/pro/cra $otop/rope/pro$OpalBinTag/lib $otop/gopal/pro/car $otop/gopal/pro/cra $otop/gopal/pro$OpalBinTag/lib $otop/geant/pro/car $otop/geant/pro/cra $otop/geant/pro$OpalBinTag/lib "

devpath=". $HOME/dev/car $HOME/dev/cra $HOME/dev$OpalBinTag/lib $HOME/dev$OpalBinTag/src $otop/rope/dev/car $otop/rope/dev/cra $otop/rope/dev$OpalBinTag/lib $otop/gopal/dev/car $otop/gopal/dev/cra $otop/gopal/dev$OpalBinTag/lib $otop/geant/dev/car $otop/geant/dev/cra $otop/geant/dev$OpalBinTag/lib $propath"

oldpath=". $HOME/car $HOME/cra $HOME$OpalBinTag/lib $HOME$OpalBinTag/src $otop/rope/old/car $otop/rope/old/cra $otop/rope/old$OpalBinTag/lib $otop/gopal/old/car $otop/gopal/old/cra $otop/gopal/old$OpalBinTag/lib $otop/geant/old/car $otop/geant/old/cra $otop/geant/old$OpalBinTag/lib "

export propath devpath oldpath

allrope="rope dc cv cj jc cz ct cx ce id lm tb pb eb pe ee em hb hp mb me mm fd od ou tr oc dd px su op si cs dx ll cu"
dstrope="rope dc px ou tr oc od cx su ll cu"
export allrope dstrope

# Set opalnews enviroment variables
#
export OPALINFO_CONFIG=$OPAL_ROOT/xoinfo/.opalinfo_config
export OPALINFO_LOCAL_CONFIG=$OPAL_ROOT/xoinfo/.opalinfo_local_config
export OPALINFO_HELP=$OPAL_ROOT/xoinfo/help
export OPALINFO_PSVIEWER="gv"
