# group settings for 'xu' (ALEPH)
# https://its.cern.ch/jira/browse/NOAFS-504

# note: this AFS directory has been archived
ALEPH_ROOT=/afs/cern.ch/aleph; export ALEPH_ROOT

OSYS=Linux; export OSYS

WWW_HOME=http://alephwww.cern.ch/; export WWW_HOME

ALEPH=$ALEPH_ROOT/$OSYS; export ALEPH

MASTER=reference; export MASTER
REFERENCE=${ALEPH_ROOT}/reference; export REFERENCE
SHARED=${ALEPH_ROOT}/shared; export SHARED

PATH="${ALEPH}/bin:${SHARED}/script:$PATH"; export PATH

TAPESCONF=$SHARED/etc/tapes.conf; export TAPESCONF

CVSROOT=$REFERENCE/cvsmaster; export CVSROOT
CVSIGNORE='HPUX9; export CVSIGNORE HPUX10 IRIX5 IRIX6 OSF1 fort.1 garbage spy *.i'

ALROOT=$REFERENCE/cvs; export ALROOT
ALINC=$ALROOT/inc; export ALINC

ADBSCONS=$ALEPH/dbase/adbscons.daf; export ADBSCONS
ADBSTEST=$ALEPH/dbase/adbstest.daf; export ADBSTEST
ADBS8990=$ALEPH/dbase/adbs8990.daf; export ADBS8990

ALEDIR=/afs/cern.ch/aleph/shared/edirlink; export ALEDIR
ALDOC=$REFERENCE/doc; export ALDOC
ALBOOK=$REFERENCE/book; export ALBOOK
PHYINC=$REFERENCE/phy; export PHYINC

BANKALFMT=$REFERENCE/dbase/bankal.fmt; export BANKALFMT
GENATTR=$REFERENCE/dbase/genattr.ddl; export GENATTR
BOSKEY=$REFERENCE/dbase/boskey.ddl; export BOSKEY

DBASBANK=$REFERENCE/phy/dbas.bank; export DBASBANK
BEAMPOSITION=$REFERENCE/phy/beam.position; export BEAMPOSITION

ALPHACARDS=$REFERENCE/phy/alpha.cards; export ALPHACARDS
GALEPHCARDS=$REFERENCE/gal/galeph.cards; export GALEPHCARDS
JULIACARDS=$REFERENCE/jul/julia.cards; export JULIACARDS
KINGALCARDS=$REFERENCE/kin/kingal.cards; export KINGALCARDS
KINAGAINCARDS=$REFERENCE/kin/kinagain.cards; export KINAGAINCARDS
LOOKCARDS=$REFERENCE/gen/look.cards; export LOOKCARDS

RUNCARTSLIST=$ALBOOK/runcarts.list; export RUNCARTSLIST

SCWORK=/aleph/scwork; export SCWORK
SCWEEK=/aleph/scweek; export SCWEEK

ALSTOUT=aldataout; export ALSTOUT
DPMSIZE=200; export DPMSIZE
DPMUSER=aleph; export DPMUSER
STAGELABEL=sl; export STAGELABEL
STAGEFSEQ=1; export STAGEFSEQ
STAGERECFM=F; export STAGERECFM
STAGELRECL=32040; export STAGELRECL
STAGEBLKSIZ=32040; export STAGEBLKSIZ
STAGECLEAN=$SHARED/script/stage-clean-sh; export STAGECLEAN
RETRYCOUNT=10; export RETRYCOUNT
ALDATA=$SHARED/data; export ALDATA
ALSTAGE=$SHARED/data; export ALSTAGE
if [ -z $SERVICE ] ; then
   DPMLINKDIR=$SHARED/data; export DPMLINKDIR
fi

CCOPT="-w -c -DALEPH_C -I${ALINC}"; export CCOPT

export VENDOR=Linux
export CPPOPT="-DUNIX -DALEPH_LINUX -I${ALINC}"
export FCOPT="-c -O -fno-automatic -fdollar-ok -fno-backslash $CPPOPT"
export FC=g77
export CCOPT=" -c -O -DALEPH_C -I${ALINC}"

ALPOPT="${FCOPT} -I${SHARED}/src/alpha/inc"; export ALOPT

GINTER="galeph.F asieve.F agtpch.F gaxeph.F"; export GINTER

GUSER="-Wl,-u,qnext_,-u,gntube_,-u,grndm_,-u,gstmed_,-u,gufld_,-u,guhadr_,-u,guphad_,-u,gusear_,-u,gustep_,-u,gutrak_,-u,gdray_,-u,gpghei_"; export GUSER
JMUID="-Wl,-u,ftrack_,-u,hmfind_,-u,mpredg_,-u,mpredm_,-u,muass_"; export JMUID
UNDEF="-Wl,-u,aboldr_,-u,babend_,-u,dafrds_"; export UNDEF


if [ -r "$ALEPH/phy/alpha.version" ]; then
  ALPVER=`egrep -i alphaver $ALEPH/phy/alpha.version | awk '{print $3}'`
else
  ALPVER='UNDEFINED_NOT_ON_AFS'
fi
if [ -r "$ALEPH/gal/galeph.version" ]; then
  GALVER=`egrep -i galver $ALEPH/gal/galeph.version | awk '{print $3}'`
else
  GALVER='UNDEFINED_NOT_ON_AFS'
fi
if [ -r "$ALEPH/jul/julia.version" ]; then
  JULVER=`egrep -i julver $ALEPH/jul/julia.version | awk '{print $3}'`
else
  JULVER='UNDEFINED_NOT_ON_AFS'
fi

BEW=/wg/wwtf/bew; export BEW
WWTF=/wg/wwtf/wwtf; export WWTF
STF=/wg/stf/stf; export STF
