# NA48 / 'vl' shell variables
# https://its.cern.ch/jira/browse/NOAFS-496
# based on /afs/cern.ch/group/vl/group_env.sh

export STAGE_HOST=castorpublic
export STAGE_SVCCLASS=na62

export CDBHOME=/afs/cern.ch/user/c/cdbase/public/CDBHOME
export CDSERV=/afs/cern.ch/na48/offline/hepdb
export DBINCDIR=/afs/cern.ch/na48/offline2/cdbase/code/inc
