# https://its.cern.ch/jira/browse/NOAFS-494
## shell settings for 'v5' (CHORUS)
# mostly from /afs/cern.ch/group/v5/group_sys.conf.sh

sys=`/usr/bin/sys`
if [ -r /chorus ]; then
  CHORUS=/chorus; export CHORUS
elif [ -r /afs/cern.ch/chorus/offl/@sys ]; then
  CHORUS=/afs/cern.ch/chorus/offl/@sys; export CHORUS
elif [ -r /afs/cern.ch/chorus/offl/$sys ]; then
  CHORUS=/afs/cern.ch/chorus/offl/$sys; export CHORUS
else
  CHORUS=/CHORUS_var_not_defined
  echo "Don't know how to find chorus software" >& 2
fi

########################################################################
# Fatmen, hepdb, stager
########################################################################
CDSERV=/hepdb/cdchorus;            export CDSERV
FMCHORUS=/fatmen/fmchorus;         export FMCHORUS
FATPATH=.:~:$CHORUS/etc;           export FATPATH
STAGE_HOST=stagepublic;          export STAGE_HOST

########################################################################
# ORACLE variables
########################################################################
CHDB_NAME=cerndb1;                 export CHDB_NAME
CHDB_ACCT=pubv5;                   export CHDB_ACCT
# removed CHDB_PASSWD

unset ORACLE_MOUNT
if [ -r /afs/cern.ch/project/oracle/@sys ]; then
    ORACLE_MOUNT=/afs/cern.ch/project/oracle/@sys
elif [ -n "$sys" ]; then
    if [ -r /afs/cern.ch/project/oracle/$sys ]; then
       ORACLE_MOUNT=/afs/cern.ch/project/oracle/$sys
    fi
fi
if [ -n "$ORACLE_MOUNT" ]; then
    export CHDB_NAME CHDB_ACCT ORACLE_MOUNT
fi

########################################################################
# Misc. interactive stuff (could even be excluded for batch)
########################################################################
CMZSYS=$CHORUS/etc/cmzsys.kumac;   export CMZSYS      # CMZ startup kumac
LESS=eMqw;                         export LESS        # default less options
TEXEDIT=emacs;                     export TEXEDIT     # Editor called from TeX

########################################################################
# Group paths
########################################################################
PATH="$CHORUS/pro/bin:$CHORUS/bin:$PATH"; export PATH

########################################################################
# Choice of Chorus software level
########################################################################

chlevel ()
{
        eval `$CHORUS/bin/chlevel.pl $1`
}
