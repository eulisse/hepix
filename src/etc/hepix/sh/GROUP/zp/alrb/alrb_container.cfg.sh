
local_alrb_mounts=""
# make all of afs available
if [ -d /afs ]; then
    set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS "-B /afs:/afs"
    local_alrb_mounts="$local_alrb_mounts /afs"
fi

# make all of eos available
if [ -d /eos ]; then
    set_ALRB_ContainerEnv ALRB_CONT_CMDOPTS "-B /eos:/eos"
    local_alrb_mounts="$local_alrb_mounts /eos"
fi

if [ "$local_alrb_mounts" != "" ]; then
    set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "echo '$local_alrb_mounts available in this container'"
fi
unset local_alrb_mounts

# set HOME to be the same as the dir on lxplus instead of /home/<user>
# don't do this now as existing container users may be confused.
#set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "export HOME='$HOME'"
# instead provide a env for users to use if they want to cd to their real HOME
set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "export UHOME='$HOME'"


