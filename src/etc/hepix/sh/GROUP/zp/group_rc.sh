# https://its.cern.ch/jira/browse/NOAFS-511
# zp / ATLAS settings, based on /afs/cern.ch/group/zp/group_env.sh

export PATH="/afs/cern.ch/atlas/scripts:$PATH"

export SITE_NAME=CERN-PROD
FRONTIER_SERVER='(serverurl=http://atlasfrontier-local.cern.ch:8000/atlr)(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(serverurl=http://ccfrontier.in2p3.fr:23128/ccin2p3-AtlasFrontier)(proxyurl=http://ca-proxy-atlas.cern.ch:3128)(proxyurl=http://ca-proxy-meyrin.cern.ch:3128)(proxyurl=http://ca-proxy.cern.ch:3128)(proxyurl=http://atlasbpfrontier.cern.ch:3127)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)'
export FRONTIER_SERVER

STAGE_HOST=castoratlas; export STAGE_HOST
STAGE_SVCCLASS=default; export STAGE_SVCCLASS

function setupATLAS
{
    if [ -d  /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ]; then

	export ALRB_localConfigDir="/etc/hepix/sh/GROUP/zp/alrb"

	export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
	return $?
    else
	\echo "Error: cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase is unavailable" >&2
	return 64
    fi
}
