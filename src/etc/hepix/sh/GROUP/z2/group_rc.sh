# z2 / ALICE shell settings
## https://its.cern.ch/jira/browse/NOAFS-507
## based on /afs/cern.ch/alice/offline/etc/login.sh

# CASTOR environment
export STAGE_HOST=castoralice
export STAGE_SVCCLASS=t0alice

# Load ALICE's environment from CernVM-FS (Bash version)
hpx_source /cvmfs/alice.cern.ch/etc/login.sh

# Latest AliPhysics and fastjet versions
LatestAliPhysics=$(alienv q 2> /dev/null | grep AliPhysics::vAN- | sort -n | tail -n1)
LatestFastjet=$(alienv q 2> /dev/null | grep fastjet:: | sort -n | tail -n1)

# Do not print any output if shell is non-interactive (i.e. scp)
tty > /dev/null 2> /dev/null || return
ac0="\033[m"
ac1="${ac0}\033[1m"
ac2="${ac0}\033[36m"
ac3="${ac0}\033[35m"
echo -e "${ac1}Welcome my dear ALICE user! To use ALICE software from CVMFS:${ac0}"
echo -e "${ac3} *${ac1} List all packages         --> ${ac2}/cvmfs/alice.cern.ch/bin/alienv q${ac0}"
echo -e "${ac3} *${ac1} List AliPhysics packages  --> ${ac2}/cvmfs/alice.cern.ch/bin/alienv q | grep -i ${ac3}aliphysics${ac0}"
echo -e "${ac3} *${ac1} Enable a specific package --> ${ac2}/cvmfs/alice.cern.ch/bin/alienv enter ${ac3}${LatestAliPhysics}${ac0}"
echo -e "${ac3} *${ac1} Enable multiple packages  --> ${ac2}/cvmfs/alice.cern.ch/bin/alienv enter ${ac3}${LatestAliPhysics}${ac2},${ac3}${LatestFastjet}${ac0}"
unset ac0 ac1 ac2 ac3 LatestAliPhysics LatestFastjet
