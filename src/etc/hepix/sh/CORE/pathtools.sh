#!/bin/sh

[ -n "$_HPX_SEEN_CORE_PATHTOOLS_SH" ] && return
_HPX_SEEN_CORE_PATHTOOLS_SH=1

# Generic sh(1)-compatible implementations of the `pathtools' functions:
# hpx_path_is_present()
# hpx_path_remove()
# hpx_path_prepend()
# hpx_path_prepend_unique()
# hpx_path_prepend_unless_present()
# hpx_path_append()
# hpx_path_append_unique()
# hpx_path_append_unless_present()


# Check if PARAM is present in PATH as a valid entry.
# Return 0 if yes, 1 if not.
hpx_path_is_present () {
	: ${1:?"hpx_path_is_present(): Missing pathname."}
	echo "$PATH" | /bin/grep -E '(^|:)'"$1"'(:|$)' > /dev/null
	return $?
}

# Remove all occurrences of a given directory from $PATH.
hpx_path_remove () {
	: ${1:?"hpx_path_remove(): Missing pathname."}
	_HPX_PATH=""
	_HPX_PATH_SEP=""

	_HPX_IFS=$IFS
	IFS=:
	for _HPX_DIR in $PATH; do
		if [ "$_HPX_DIR" != "$1" ]; then
			_HPX_PATH=${_HPX_PATH}${_HPX_PATH_SEP}${_HPX_DIR}
		fi
		_HPX_PATH_SEP=":"
	done

	IFS=$_HPX_IFS
	unset _HPX_IFS
	PATH=$_HPX_PATH

	unset _HPX_PATH
	unset _HPX_PATH_SEP
	unset _HPX_DIR
}

# Prepend given directoy to PATH.
hpx_path_prepend () {
	: ${1:?"hpx_path_prepend(): Missing pathname."}
	[ -z "$PATH" ] && PATH="$1" || PATH="$1:$PATH"
}

# Prepend given directory to PATH and remove all other
# occurrences.
hpx_path_prepend_unique () {
	: ${1:?"hpx_path_prepend_unique(): Missing pathname."}
	hpx_path_remove "$1"
	hpx_path_prepend "$1"
}

# Prepend given directory to PATH unless it is already
# present (anywhere) in PATH.
# NOTE(fuji): The if clause is written as such because Solaris sh(1) cannot
# understand `!' as `negate condition'.
hpx_path_prepend_unless_present () {
	: ${1:?"hpx_path_prepend_unless_present(): Missing pathname."}
	if hpx_path_is_present "$1"; then
		:
	else
		hpx_path_prepend "$1"
	fi
}

# Append given directoy to PATH.
hpx_path_append () {
	: ${1:?"hpx_path_append(): Missing pathname."}
	[ -z "$PATH" ] && PATH="$1" || PATH="$PATH:$1"
}

# Append given directory to PATH and remove all other
# occurrences.
hpx_path_append_unique () {
	: ${1:?"hpx_path_append_unique(): Missing pathname."}
	hpx_path_remove "$1"
	hpx_path_append "$1"
}

# Append given directory to PATH unless it is already
# present (anywhere) in PATH.
# NOTE(fuji): The if clause is written as such because Solaris sh(1) cannot
# understand `!' as `negate condition'.
hpx_path_append_unless_present () {
	: ${1:?"hpx_path_append_unless_present(): Missing pathname."}
	if hpx_path_is_present "$1"; then
		:
	else
		hpx_path_append "$1"
	fi
}

# End of file.
