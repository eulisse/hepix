# CMS-specifics
## https://its.cern.ch/jira/browse/NOAFS-510
## /afs/cern.ch/group/zh/group_env.csh

# This is host dependent. If CvmFS is installed, use it, otherwise fall 
# back on AFS (e.g. on a desktop/laptop w/o CvmFS).
if ( -d /cvmfs/cms.cern.ch ) then
	setenv CMS_PATH /cvmfs/cms.cern.ch
else
	setenv CMS_PATH /afs/cern.ch/cms
endif

# CMS wide settings for latex styles
setenv TEXINPUTS :$CMS_PATH/latex/styles

# setup for castor
setenv STAGE_HOST castorcms

hpx_source $CMS_PATH/cmsset_default.csh
