# group settings for 'xu' (ALEPH)
# https://its.cern.ch/jira/browse/NOAFS-504

# note: this AFS directory has been archived
setenv ALEPH_ROOT /afs/cern.ch/aleph

setenv OSYS Linux

setenv WWW_HOME http://alephwww.cern.ch/

setenv ALEPH $ALEPH_ROOT/$OSYS

setenv MASTER reference
setenv REFERENCE ${ALEPH_ROOT}/reference
setenv SHARED ${ALEPH_ROOT}/shared

setenv PATH "${ALEPH}/bin:${SHARED}/script:$PATH"

setenv TAPESCONF $SHARED/etc/tapes.conf

setenv CVSROOT $REFERENCE/cvsmaster
setenv CVSIGNORE 'HPUX9 HPUX10 IRIX5 IRIX6 OSF1 fort.1 garbage spy *.i'

setenv ALROOT $REFERENCE/cvs
setenv ALINC $ALROOT/inc

setenv ADBSCONS $ALEPH/dbase/adbscons.daf
setenv ADBSTEST $ALEPH/dbase/adbstest.daf
setenv ADBS8990 $ALEPH/dbase/adbs8990.daf

setenv ALEDIR /afs/cern.ch/aleph/shared/edirlink
setenv ALDOC $REFERENCE/doc
setenv ALBOOK $REFERENCE/book
setenv PHYINC $REFERENCE/phy

setenv BANKALFMT $REFERENCE/dbase/bankal.fmt
setenv GENATTR $REFERENCE/dbase/genattr.ddl
setenv BOSKEY $REFERENCE/dbase/boskey.ddl

setenv DBASBANK $REFERENCE/phy/dbas.bank
setenv BEAMPOSITION $REFERENCE/phy/beam.position

setenv ALPHACARDS $REFERENCE/phy/alpha.cards
setenv GALEPHCARDS $REFERENCE/gal/galeph.cards
setenv JULIACARDS $REFERENCE/jul/julia.cards
setenv KINGALCARDS $REFERENCE/kin/kingal.cards
setenv KINAGAINCARDS $REFERENCE/kin/kinagain.cards
setenv LOOKCARDS $REFERENCE/gen/look.cards

setenv RUNCARTSLIST $ALBOOK/runcarts.list

setenv SCWORK /aleph/scwork
setenv SCWEEK /aleph/scweek

setenv ALSTOUT aldataout
setenv DPMSIZE 200
setenv DPMUSER aleph
setenv STAGELABEL sl
setenv STAGEFSEQ 1
setenv STAGERECFM F
setenv STAGELRECL 32040
setenv STAGEBLKSIZ 32040
setenv STAGECLEAN $SHARED/script/stage-clean-sh
setenv RETRYCOUNT 10
setenv ALDATA $SHARED/data
setenv ALSTAGE $SHARED/data
if ( $?SERVICE)  then
   setenv DPMLINKDIR $SHARED/data
endif

setenv CCOPT "-w -c -DALEPH_C -I${ALINC}"

setenv VENDOR Linux
setenv CPPOPT "-DUNIX -DALEPH_LINUX -I${ALINC}"
setenv FCOPT  "-c -O -fno-automatic -fdollar-ok -fno-backslash $CPPOPT"
setenv FC    g77
setenv CCOPT " -c -O -DALEPH_C -I${ALINC}"

setenv ALPOPT "${FCOPT} -I${SHARED}/src/alpha/inc"

setenv GINTER "galeph.F asieve.F agtpch.F gaxeph.F"

setenv GUSER "-Wl,-u,qnext_,-u,gntube_,-u,grndm_,-u,gstmed_,-u,gufld_,-u,guhadr_,-u,guphad_,-u,gusear_,-u,gustep_,-u,gutrak_,-u,gdray_,-u,gpghei_"
setenv JMUID "-Wl,-u,ftrack_,-u,hmfind_,-u,mpredg_,-u,mpredm_,-u,muass_"
setenv UNDEF "-Wl,-u,aboldr_,-u,babend_,-u,dafrds_"


if ( -r "$ALEPH/phy/alpha.version" ) then
  setenv ALPVER `egrep -i alphaver $ALEPH/phy/alpha.version | awk '{print $3}'`
else
  setenv ALPVER 'UNDEFINED_NOT_ON_AFS'
endif
if ( -r "$ALEPH/gal/galeph.version" ) then
  setenv GALVER `egrep -i galver $ALEPH/gal/galeph.version | awk '{print $3}'`
else
  setenv GALVER 'UNDEFINED_NOT_ON_AFS'
endif
if ( -r "$ALEPH/jul/julia.version" ) then
  setenv JULVER `egrep -i julver $ALEPH/jul/julia.version | awk '{print $3}'`
else
  setenv JULVER 'UNDEFINED_NOT_ON_AFS'
endif

setenv BEW /wg/wwtf/bew
setenv WWTF /wg/wwtf/wwtf
setenv STF /wg/stf/stf
