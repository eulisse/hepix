# group settings for 'ws' (OPAL)
# https://its.cern.ch/jira/browse/NOAFS-501
# based on /afs/cern.ch/group/ws/group_env.csh

setenv OPAL /afs/cern.ch/opal     ##Define the top of the OPAL project tree
setenv OPAL_ROOT $OPAL/offline    ##Define the top of the OPAL code tree
setenv OPAL_WWW $OPAL/www         ##Define the top of the OPAL WWW tree
setenv OPAL_GEN $OPAL/offline/generator ##Define the top of the
                                            ##OPAL generator tree
setenv otop $OPAL/offline ##Define the top of the OPAL code tree
setenv odoc $OPAL/www/doc ##Define the top of the OPAL documentation
setenv opal $OPAL     ##Define the top of the OPAL project tree
setenv OPAL_GR $OPAL/offline/grope ## the grope directory

setenv CERN_ROOT /cern/pro

## Define CERN global variables (wrong as of SLC5, does anybody care?????)
setenv libver "pro"
setenv gksver "pro"
setenv cern /cern
setenv cern_root $cern/$libver
setenv kernlib $cern_root/lib/libkernlib.a
setenv packlib $cern_root/lib/libpacklib.a
setenv mathlib $cern_root/lib/libmathlib.a
setenv graflib $cern_root/lib/libgraflib.a
setenv graflibX11 $cern_root/lib/libgrafX11.a

## Generate OPAL symbols <version_xx>
setenv version_opal pro
if ( -f $otop/utl/oversion.$version_opal.csh ) then
        hpx_source $otop/utl/oversion.$version_opal.csh
else
        echo "Warning: oversion.$version_opal.csh does not exist!"
endif

setenv OPAL_P_CAR $otop/links/pro
setenv OPAL_P_CRA $otop/links/pro
setenv OPAL_P_LIB $otop/links/pro
setenv OPAL_D_CAR $otop/links/dev
setenv OPAL_D_CRA $otop/links/dev
setenv OPAL_D_LIB $otop/links/dev

setenv OPAL_P_DB $otop/db

setenv CERN_P_CAR /cern/pro/src/car
setenv CERN_P_LIB /cern/pro/lib

setenv FMOPAL $OPAL/fatmen/fmopal

# set the hardware variable, needed for some scripts (ytofort, fort, link, ...)
setenv HARDWARE `$otop/scripts/hardware`
# 
if ( $HARDWARE == "i386slc3" ) then
    set OpalBinTag = '/i386slc3'
else if ( $HARDWARE == "i386slc5" ) then
    set OpalBinTag = '/i386slc5'
else if ( $HARDWARE == "amd64slc5" ) then
    set OpalBinTag = '/amd64slc5'
else if ( $HARDWARE == "i386slc6" || $HARDWARE == "amd64slc6" ) then
    set OpalBinTag = '/i386slc6'
    setenv PATH "${PATH}:/cern/pro/bin"
else if ( $HARDWARE == "i386cc7" || $HARDWARE == "amd64cc7" ) then
    set OpalBinTag = '/i386cc7'
    setenv PATH "${PATH}:/cern/pro/bin"
else
    set OpalBinTag = ''
endif


setenv PATH "${otop}${OpalBinTag}/mgr:$otop/scripts:$otop/bin:$PATH"

setenv propath ". $HOME/car $HOME/cra ${HOME}${OpalBinTag}/lib ${HOME}${OpalBinTag}/src $otop/rope/pro/car $otop/rope/pro/cra $otop/rope/pro${OpalBinTag}/lib $otop/gopal/pro/car $otop/gopal/pro/cra $otop/gopal/pro${OpalBinTag}/lib $otop/geant/pro/car $otop/geant/pro/cra $otop/geant/pro${OpalBinTag}/lib "

setenv devpath ". $HOME/dev/car $HOME/dev/cra $HOME/dev${OpalBinTag}/lib $HOME/dev${OpalBinTag}/src $otop/rope/dev/car $otop/rope/dev/cra $otop/rope/dev${OpalBinTag}/lib $otop/gopal/dev/car $otop/gopal/dev/cra $otop/gopal/dev${OpalBinTag}/lib $otop/geant/dev/car $otop/geant/dev/cra $otop/geant/dev${OpalBinTag}/lib $propath"

setenv oldpath ". $HOME/car $HOME/cra ${HOME}${OpalBinTag}/lib $otop/rope/old/car $otop/rope/old/cra $otop/rope/old${OpalBinTag}/lib $otop/gopal/old/car $otop/gopal/old/cra $otop/gopal/old${OpalBinTag}/lib $otop/geant/old/car $otop/geant/old/cra $otop/geant/old${OpalBinTag}/lib "

#
setenv allrope "rope dc cv cj jc cz ct cx ce id lm tb pb eb pe ee em hb hp mb me mm fd od ou tr oc dd px su op si cs dx ll cu"
setenv dstrope "rope dc px ou tr oc od cx su ll cu"

# Set opalnews enviroment variables
#
setenv OPALINFO_CONFIG $OPAL_ROOT/xoinfo/.opalinfo_config
setenv OPALINFO_LOCAL_CONFIG $OPAL_ROOT/xoinfo/.opalinfo_local_config
setenv OPALINFO_HELP $OPAL_ROOT/xoinfo/help
setenv OPALINFO_PSVIEWER "gv"
