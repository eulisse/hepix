# z2 / ALICE shell settings
## https://its.cern.ch/jira/browse/NOAFS-507
## based on /afs/cern.ch/alice/offline/etc/login.csh

# Load ALICE's environment from CernVM-FS (C-Shell)
hpx_source /cvmfs/alice.cern.ch/etc/login.csh

# CASTOR environment
setenv STAGE_HOST castoralice
setenv STAGE_SVCCLASS t0alice

# Latest AliPhysics and fastjet versions
setenv LatestAliPhysics `alienv q | grep AliPhysics::vAN- | sort -n | tail -n1`
setenv LatestFastjet `alienv q | grep fastjet:: | sort -n | tail -n1`

# Do not print any output if shell is non-interactive (i.e. scp)
tty -q >& /dev/null
if ( $? == 0 ) then
  setenv ac0 "\033[m"
  setenv ac1 "${ac0}\033[1m"
  setenv ac2 "${ac0}\033[36m"
  setenv ac3 "${ac0}\033[35m"
  echo "${ac1}Welcome my dear ALICE user! To use ALICE software from CVMFS:${ac0}"
  echo "${ac3} *${ac1} List all packages         --> ${ac2}alienv q${ac0}"
  echo "${ac3} *${ac1} List AliPhysics packages  --> ${ac2}alienv q | grep -i ${ac3}aliphysics${ac0}"
  echo "${ac3} *${ac1} Enable a specific package --> ${ac2}alienv enter ${ac3}${LatestAliPhysics}${ac0}"
  echo "${ac3} *${ac1} Enable multiple packages  --> ${ac2}alienv enter ${ac3}${LatestAliPhysics}${ac2},${ac3}${LatestFastjet}${ac0}"
  unsetenv ac0 ac1 ac2 ac3 LatestAliPhysics LatestFastjet
endif
