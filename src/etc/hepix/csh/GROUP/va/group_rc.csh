# https://its.cern.ch/jira/browse/NOAFS-495
## shell settings for "va" (AMS)

setenv OS "Linux"
setenv LD_LIBRARY_PATH "/afs/cern.ch/ams/local2/opt/xrootd-gcc64-41/lib64"
if( -e /opt/intel/Compiler/11.0/069/ ) then
  setenv PATH "/opt/intel/Compiler/11.0/069/bin/ia32:${PATH}"
  setenv LD_LIBRARY_PATH "/opt/intel/Compiler/11.0/069/lib:${LD_LIBRARY_PATH}"
endif

setenv STAGE_HOST castorpublic
setenv STAGE_SVCCLASS amsuser
setenv CASTOR_INSTANCE castorpublic

limit memoryuse 2000000
limit vmemoryuse 3000000
