# https://its.cern.ch/jira/browse/NOAFS-491
#
# SL/AP Group `sys.conf.csh' Environment  --- BAT :-)
#
setenv AP_PROJECT_DIR /afs/cern.ch/project/slap
#
# SL/AP Group Path
#
setenv PATH "$AP_PROJECT_DIR/bin:$AP_PROJECT_DIR/scripts:$PATH"
#
if (! $?AP_GROUP_CONF) then
  #
  # SL/AP Manual Path
  #
  if ( $?MANPATH ) then
    setenv MANPATH $AP_PROJECT_DIR/man:$MANPATH
  else
    setenv MANPATH $AP_PROJECT_DIR/man
  endif
  #
  if ( ! $?TEXMFS ) setenv TEXMFS "{$AP_PROJECT_DIR/share/texmf,"'\!\!$TEXMF}'
  #
  setenv AP_GROUP_CONF true
endif
#
# SL/AP HTTPd Server Root Directory
#
setenv AP_WWW_HOME_DIR $AP_PROJECT_DIR/etc/httpd
setenv AP_HARVEST_HOME $AP_PROJECT_DIR/etc/harvest
#
# LHC project directory 
#
setenv LHC_DIR /afs/cern.ch/eng/lhc
