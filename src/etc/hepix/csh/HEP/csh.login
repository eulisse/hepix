#!/bin/csh

if ( $?_HPX_SEEN_HEP_CSH_LOGIN ) then
	exit
endif
set _HPX_SEEN_HEP_CSH_LOGIN=1

if ( ! $?HPX_INIT ) then
	source /etc/hepix/init.csh
endif

hpx_source $HPX_HOME_HEP/csh.cshrc

if ( -f /etc/nologin && "$USER" != "user" ) then
	hpx_echo "N: Sorry, system is under maintenance, no logins allowed."
	/bin/cat /etc/nologin
	hpx_echo "N: Sleeping 5 seconds."
	/bin/sleep 5
	exit
endif

if ( -s /etc/hepix/motd ) then
	/bin/cat /etc/hepix/motd
endif

if ( "x$HPX_AFS_HOME" != x ) then
	alias _hpx_afs_quota_check "source $HPX_HOME_CORE/_hpx_afs_quota_check.csh "
	if ( -x /usr/bin/fs ) then
		set HPXB_FS=/usr/bin/fs
	endif
	if ( $?HPXB_FS ) then
		# Execute fs lq ~ command and extract numbers from output.
		# The shell could do the whitespace split but fs(1) is
		# altering output on quota-near-full conditions.
		# NOTE(fuji): csh/tcsh is uncapable of redirecting stderr only.
		set _HPX_LQ=`$HPXB_FS lq $HOME`

		# fs(1) would complain loudly of access violation if
		# logging in without a token.
		if ( $status == 0 ) then
			hpx_debug $_HPX_LQ
			set _HPX_LQ=`echo $_HPX_LQ | /usr/bin/xargs | \
				/bin/sed -n '1s/^[^.]*\.[^ ]* [^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\)%[^0-9]*\([0-9]*\)%.*$/\1 \2 \3 \4/p'`
			hpx_debug $_HPX_LQ
			_hpx_afs_quota_check $_HPX_LQ
		else
			hpx_echo "E: $HPXB_FS returned error, no tokens?"
		endif
		unset _HPX_LQ
	else
		hpx_echo "E: Unable to check AFS quota, no fs(1) binary?"
	endif
endif

# Personal temporary directory.
set HPX_TMPDIR=/tmp/$USER
if ( -d $HPX_TMPDIR ) then
	/usr/bin/test -O $HPX_TMPDIR >/dev/null
	if ( $status == 0 ) then
		setenv TMPDIR $HPX_TMPDIR
	endif
endif
if ( ! $?TMPDIR ) then
        set HPX_UMASK=`umask`
	umask 0077
	/bin/mkdir $HPX_TMPDIR >/dev/null
	if ( $status == 0 ) then
		setenv TMPDIR $HPX_TMPDIR
	endif
	umask $HPX_UMASK
        unset HPX_UMASK
	if ( $?TMPDIR ) then
		hpx_debug "Created $HPX_TMPDIR."
	endif
endif
unset HPX_TMPDIR

# End of file.
