%{!?dist: %define dist .slc6}
%define debug_package %{nil}

Summary: HEPiX login scripts
Name: hepix
Version: 4.10.1
Release: 0%{?dist}
BuildArch: noarch
Source: %{name}-%{version}.tgz
Group: CERN/Utilities
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Packager: Linux.Support@cern.ch
Vendor: CERN
License: GPL

%description
The HEPiX login scripts customize your shell environment
to provide a unique feeling across HEP sites.

%prep
%setup -q

%install
mkdir -p ${RPM_BUILD_ROOT}
cp -r etc ${RPM_BUILD_ROOT}/etc
mkdir -p ${RPM_BUILD_ROOT}/usr/share/man/man1
cp doc/hepix.1 ${RPM_BUILD_ROOT}/usr/share/man/man1

# Hepic v4 cleanup
/bin/rm -rf ${RPM_BUILD_ROOT}/etc/hepix/oracle_env.{,c}sh
/bin/rm -rf ${RPM_BUILD_ROOT}/etc/hepix/sh/SITE
/bin/rm -rf ${RPM_BUILD_ROOT}/etc/hepix/csh/SITE

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr (-, root, root)
/etc
/usr
%doc doc/README doc/ChangeLog doc/NEWS doc/TODO

%changelog
* Wed Nov 4 2020 Jan Iven <jan.iven@cern.ch> - 4.10.1
- remove references to AFS /afs/cern.ch/group and related 'fall-through' options
- avoid warning for non-existing /afs/cern.ch/aleph for ALEPH/xu

* Wed Dec 16 2019 Jan Iven <jan.iven@cern.ch> - 4.9.10
- update FRONTIER_SERVER variable for ATLAS/zp

* Fri Dec 06 2019 Ben Morrice <ben.morrice@cern.ch> - 4.9.9
- add simple fix for egrep on el8

* Tue Sep 10 2019 Jan Iven <jan.iven@cern.ch> - 4.9.8
- ALICE/z2 non-interactive 'csh' fix from S.Traylen

* Wed May 29 2019 Jan Iven <jan.iven@cern.ch> - 4.9.7
- ATLAS/zp changes for in-container filesystem access

* Thu Apr 25 2019 Jan Iven <jan.iven@cern.ch> - 4.9.6
- DELPHI/xx now uses only CVMFS

* Wed Apr 3 2019 Jan Iven <jan.iven@cern.ch> - 4.9.5
- add group scripts for 'z5'

* Tue Oct 2 2018 Jan Iven <jan.iven@cern.ch> - 4.9.4-0
- fix HPX_WANTS_AFS for tcsh; remove periodic AFS token checks; interactive shells no longer source AFS unless specified

* Thu Jun 14 2018  Jan Iven <jan.iven@cern.ch> - 4.9.2-0
- fix CVMFS location for 'wj'

* Wed May 23 2018  Jan Iven <jan.iven@cern.ch> - 4.9.1-0
- remove support for 'obsolete space-separated entries' in USERPATH GROUPPATH
- add a first round of group scripts to the RPM: si t3 v5 va vv vy wj xv xx z2 zh zp

* Mon Nov 27 2017  Cristian Contescu <acontesc@cern.ch> - 4.0.14-1
- Add hook to allow sourcing of computing group scripts from /etc/hepix/{c,}sh/GROUP

* Fri Jul 27 2012  Thomas Oulevey  <thomas.oulevey@cern.ch> - 4.0.13-1.slc6
- Change low space message warning

- Tue Mar 27 2012  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.13-0.slc6
- eliminate internal variable $HPX_SYSTEM_HOME
- empty file /etc/hepix/motd

* Mon Jan 16 2012  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.12-0.slc6
- bug fix: typo in /etc/hepix/csh/HEP/csh.cshrc

* Mon Jan  9 2012  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.11-0.slc6
- re-introduce EDITOR variable (Trac #34)

* Fri Dec  2 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.10-0.slc6
- bug fix: typo in /etc/hepix/csh/HEP/csh.cshrc

* Fri Nov 23 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.9-0.slc6
- Fifth round of cleanups

* Fri Nov  4 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.8-0.slc6
- Fourth round of cleanups

* Fri Nov  4 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.7-0.slc6
- Third round of cleanups
  - reshuffle /etc/hepix/init.{,c}sh files

* Tue Nov  1 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.6-0.slc6
- Second round of cleanups
  - remove /etc/hepix/{,c}sh/SITE directories

* Fri Oct 28 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.4-0.slc6
- No longer massage MANPATH
- Fix broken GROUPPATH

* Tue Oct 25 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.0.3-0.slc6
- Major cleanup: remove all non-CERN-specific bits, only retain:
  - sourcing of group environments under /afs/cern.ch/group/XX
  - some CERN-specific environment variables

* Tue Aug 30 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 3.10.1-1.slc6
- fix for (rare...) case where `fs lq ~` output is not properly handled. See INC039175
- More cleanups

* Mon Jun  6 2011  Jan van Eldik <Jan.van.Eldik@cern.ch> - 3.10.0-1.slc6
- Remove obsolete environment variables:
  - XFILESEARCHPATH (points to non-existent directories on SLC6)
  - HPSS_HOME (and HSM_HOME)
  - NNTPSERVER
  - WWW_HOME
  - PRINT_CMD
- Redefine EDITOR="nano -w" (was: "pico -w")
- Drop documentation under /usr/share/doc/hepix-%{version}/old/
- Integrate into LinuxSupport Trac

* Wed Apr  2 2008  KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.10-1.cern
- Properly fix $TERM deref protection in csh flavors.

* Tue Mar 18 2008 Jan Iven <jan.iven@cern.ch> - 3.9.9-6.cern
- fix bug in SITE/tcshrc, csh.login (undefined TERM variable referenced, M.Litmaath/SAM test)

* Tue Feb 26 2008 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.9-5.cern
- 3.9.9-3.cern was never released.
- 3.9.9-4.cern was never released.
- Take out the compatibility links *again*.
- Introduce hepix.1 manpage (thanks Jan).
- Include original documentation.

* Fri Jun 16 2006 Jan Iven <jan.iven@cern.ch> 3.9.9-3.cern
- put back the /usr/local/ links that we dropped silently, this time as part of the RPM

* Wed Jun  7 2006  KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.9-2.cern
- This is not my day... release testing is not really standardized.

* Wed Jun  7 2006  KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.9-1.cern
- Properly bump version number.

* Wed Jun  7 2006  KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.9-0.cern
- NOTE: PATH does not contain `.' (dot) anymore!
- Assorted fixes, see ChangeLog for details.

* Tue Jan 18 2005 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.8-3.cern
- Fix compatibility links for pre-3.9.0 versions.

* Mon Jan 17 2005 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.8-2.cern
- Adhere to RedHat policy of sourcing /etc/profile.d/* scripts.

* Fri Jan 14 2005 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.8-1.cern
- Fix HPX_HEPIX=OFF typo.

* Thu Jan 13 2005 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.8-0.cern
- HEPiX 3.9.8 released.

* Tue Oct 19 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.7-4.cern
- Fix batch environment.

* Mon Sep 20 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.7-3.cern
- Fix csh undefined variable problem with GROUPPATH/USERPATH compatibility
  layer.

* Mon Sep 20 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.7-2.cern
- Fix _HPX_LQ typo.

* Mon Sep 20 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.7-1.cern
- Hotfix for backtick flattening.

* Mon Sep 20 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.7-0.cern
- Various fixes.

* Tue Aug  3 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.6-0.cern
- Fix PATH merging.

* Thu Jul 15 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.5-2.cern
- Fix up expr non-portable BRE.

* Fri Jul 11 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.5-1.cern
- Hotfix for retaining PATH.

* Thu Jul 10 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.5-0.cern
- HEPiX 3.9.5 released.

* Thu Jul 10 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.4-1.cern
- Compatibility links set up in %post for pre-3.9.0 installations.

* Thu Jul  8 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.4-0.cern
- HEPiX 3.9.4 released.

* Wed Jun 16 2004 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3.9.3-0.cern
- HEPiX 3.9.3 released.

* Mon May  3 2004 Peter.Kelemen@cern.ch
- Build RPM as root, all bets off.

* Mon May  3 2004 Peter.Kelemen@cern.ch
- Position of %defattr makes a difference?

* Mon May  3 2004 Peter.Kelemen@cern.ch
- HEPiX 3.9.2 released.
- Fixes a brown paperbag bug in csh/SITE/csh.cshrc.

* Mon May  3 2004 Peter.Kelemen@cern.ch
- HEPiX 3.9.1 released.
- Fixes all outstanding bugs.

* Tue Mar  2 2004 Peter.Kelemen@cern.ch
- Let RPM figure out the list of files included in the package.

* Tue Mar  2 2004 Peter.Kelemen@cern.ch
- HEPiX 3.9.0-8.cern released (no code changes).
- Added defattr directive.

* Fri Feb 27 2004 Peter.Kelemen@cern.ch
- Introduce architecture (noarch).
- Make sure RPM_BUILD_ROOT exists by creating it.

* Tue Aug 19 2003 Peter.Kelemen@cern.ch
- initial build

# End of file.
